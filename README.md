Repository to store source scripts for [macrobenchmark](https://gitlab.com/jangorecki/macrobenchmark) R package.  
  
Rraw scripts are executed against range of git commits from R package git tree.

```sh
Rscript -e 'install.packages("macrobenchmark", repos="https://jangorecki.gitlab.io/macrobenchmark")'
git clone https://gitlab.com/jangorecki/macrobenchmarking.git
cd macrobenchmarking
# install old data.table dependencies
Rscript -e 'install.packages(c("reshape2","chron"), repos="https://cloud.r-project.org")'
# run macrobenchmark
Rscript data.table/data.table.R
```

When benchmarking newer commits from latest devel data.table later on you can `git pull` from `https://www.github.com/Rdatatable/data.table.git` to `macrobenchmarking/cache/data.table` or `rm -r cache/data.table` to re-clone it from `data.table/data.table.R` script.  